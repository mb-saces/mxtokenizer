// Copyright (C) 2022 saces@c-base.org
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package main

import (
	"fmt"
	"os"
	"time"

	"gitlab.com/mb-saces/mxtokenizer/goutensil"
	"golang.org/x/term"
	"maunium.net/go/mautrix"
	"maunium.net/go/mautrix/id"
)

// Make a variable for the version which will be set at build time.
var version = "unknown"

func print_usage() {
	fmt.Printf("mxtokenizer version %s\n", version)
	fmt.Println("Usage: mxtokenizer MXID")
}

func input_password() string {
	fmt.Print("Enter Password: ")
	bytePassword, err := term.ReadPassword(int(os.Stdin.Fd()))
	fmt.Println()
	goutensil.MaybePanic(err)
	return string(bytePassword)
}

func main() {
	if len(os.Args) != 2 {
		print_usage()
		os.Exit(1)
	}

	mxid := id.UserID(os.Args[1])

	_, hs, err := mxid.Parse()

	goutensil.MaybePanic(err)

	wk, err := mautrix.DiscoverClientAPI(hs)
	goutensil.MaybePanic(err)
	if wk != nil {
		// no wellknown
		hs = wk.Homeserver.BaseURL
	}

	mauclient, err := mautrix.NewClient(hs, mxid, "")
	goutensil.MaybePanic(err)

	deviceName := fmt.Sprintf("mxtokenizer-%d", time.Now().Unix())

	resp, err := mauclient.Login(&mautrix.ReqLogin{
		Type: "m.login.password",
		Identifier: mautrix.UserIdentifier{
			Type: "m.id.user",
			User: mxid.Localpart(),
		},
		Password:                 input_password(),
		DeviceID:                 id.DeviceID(deviceName),
		InitialDeviceDisplayName: deviceName,
		StoreCredentials:         false,
		StoreHomeserverURL:       false,
	})
	goutensil.MaybePanic(err)

	fmt.Println(resp.AccessToken)
}
