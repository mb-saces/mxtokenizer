package goutensil

import (
	"errors"
	"strings"
)

// IsYes returns true if the string contains a form of 'yes'
func IsYes(s string) bool {
	switch {
	case strings.HasPrefix("yes", s):
		fallthrough
	case strings.HasPrefix("ja", s):
		fallthrough
	case strings.HasPrefix("tak", s):
		fallthrough
	case strings.HasPrefix("да", s):
		return true
	default:
		return false
	}
}

// IsNo returns true if the string contains a form of 'no'
func IsNo(s string) bool {
	switch {
	case strings.HasPrefix("non", s):
		fallthrough
	case strings.HasPrefix("nein", s):
		fallthrough
	case strings.HasPrefix("нет", s):
		fallthrough
	case strings.HasPrefix("nie", s):
		return true
	default:
		return false
	}
}

// IsTrue returns true if the string contains a form of 'true'
func IsTrue(s string) bool {
	switch {
	case strings.HasPrefix("wahr", s):
		fallthrough
	case strings.HasPrefix("true", s):
		return true
	default:
		return false
	}
}

// IsFalse returns true if the string contains a form of 'false'
func IsFalse(s string) bool {
	switch {
	case strings.HasPrefix("falsch", s):
		fallthrough
	case strings.HasPrefix("false", s):
		return true
	default:
		return false
	}
}

// String2Bool parses the human input to bool
// if the input could not be parsed an error is returned
func String2Bool(s string) (bool, error) {
	s = strings.TrimSpace(s)
	switch s {
	case "0":
		return false, nil
	case "1":
		return true, nil
	}

	s = strings.ToLower(s)

	switch {
	case IsYes(s):
		fallthrough
	case IsTrue(s):
		return true, nil
	case IsNo(s):
		fallthrough
	case IsFalse(s):
		return false, nil
	default:
		return false, errors.New("input could not be parsed to bool")
	}
}
